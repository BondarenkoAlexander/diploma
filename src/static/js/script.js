window.addEventListener('DOMContentLoaded', () => {
    $preloader = $('.preloader'),
    $loader = $preloader.find('.preloadImg');
    $loader.delay(2550).fadeOut('slow');
    $preloader.delay(2050).fadeOut('slow');

    function setTableHeight() {
        if (document.querySelector('.journal__table')) {
            document.querySelector('.journal__table').style.maxHeight = document.body.scrollHeight - document.querySelector('.journal__table').offsetTop - 220 + 'px';
        }
        if (document.querySelector('.shedule__table')) {
            $('.shedule__table').css('maxHeight', $(window).height() - $('.shedule__table').offset().top - 40);
        }
    }
    let btn = document.querySelector('.header__tabHide'),
        menu = document.querySelector('.menu'),
        content = document.querySelector('.content'),
        menuItem = document.querySelectorAll('.menu__item'),
        menuList = document.querySelectorAll('.menu__list a'),
        menuTitle = document.querySelector('.menu__title'),
        menuLogo = document.querySelector('.menu__logo');

    function smallTab() {
        if (btn) {
            btn.classList.add('hideTabMenuActive');
            menu.style.width = '80px';
            content.style.marginLeft = '80px';
            menuList.forEach(item => {
                item.childNodes[2].nextSibling.style.display = 'none';
            });
            menuTitle.style.display = 'none';
            menuLogo.style.display = 'block';
            menuItem.forEach(item => {
                item.classList.add('smallMenuItem');
            })
            localStorage.setItem('hideTabMenuActive', 'true');
        }
    }

    function bigTab() {
        if (btn) {
            btn.classList.remove('hideTabMenuActive');
            menu.style.width = '330px';
            content.style.marginLeft = '330px';
            setTimeout(() => {
                menuList.forEach(item => {
                    item.childNodes[2].nextSibling.style.display = 'inline-block';
                });
                menuTitle.style.display = 'block';
                menuLogo.style.display = 'none';
                menuItem.forEach(item => {
                    item.classList.remove('smallMenuItem');
                })
                localStorage.setItem('hideTabMenuActive', 'false');
            }, 200)
        }
    }

    function hideTabMenu() {
        if (btn) {
            btn.addEventListener('click', () => {
                if (!btn.classList.contains('hideTabMenuActive')) {
                    smallTab();
                } else {
                    bigTab();
                }
            })
        }
    }
    hideTabMenu();

    function registerBtn() {
        let btn = document.querySelector('.register__btn');
        if (btn) {
            btn.addEventListener('click', () => {
                localStorage.setItem('hideTabMenuActive', 'false');
            })
        }
    }
    registerBtn();

    function checkLoginInputs() {
        let inputs = document.querySelectorAll('.register__item input')
        inputs.forEach(item => {
            console.log(item.value)
            if (item.autocomplete == 'username' || item.autocomplete == 'current-password' || item.value != '') {
                item.classList.add('has-val')
            }
        })
    }
    checkLoginInputs();

    onload = function() {
        if (localStorage.getItem('hideTabMenuActive') == 'true') {
            smallTab();
        } else if (localStorage.getItem('hideTabMenuActive') == 'false') {
            bigTab();
        }

        if ($("select[name='subject_name']")) {
            $("select[name='subject_name']").find(`option:contains(${localStorage.getItem('subjectJournalName')})`).attr("selected", "selected")
        }
        if ($("select[name='group_name']")) {
            $("select[name='group_name']").find(`option:contains(${localStorage.getItem('groupJournalName')})`).attr("selected", "selected")
        }

        var lnk = document.querySelectorAll('.menu__item');
        let currentLink = window.location.pathname;
        currentLink = currentLink.split('/')[1];
        for (var j = 0; j < lnk.length; j++) {
            if (lnk[j].href.includes(window.location.pathname) && lnk[j].href == (localStorage.getItem('menuLink'))) {
                lnk[j].classList.add('menu__item--active');
            } else if (lnk[j].href == (window.location.href)) {
                lnk[j].classList.add('menu__item--active');
            }
        }
    }

    function setSubjectAndGroupInJournal() {
        let btn = document.querySelector('.shedule__item--openMarkBook');
        if (btn) {
            btn.addEventListener('click', () => {
                let subject = document.getElementsByName('subject_name')[0].options[document.getElementsByName('subject_name')[0].selectedIndex].text.trim();
                if (document.getElementsByName('group_name')[0]) {
                    let group = document.getElementsByName('group_name')[0].options[document.getElementsByName('group_name')[0].selectedIndex].text.trim();
                }
                localStorage.setItem('groupJournalName', group);
                localStorage.setItem('subjectJournalName', subject);
            })
        }
    }
    setSubjectAndGroupInJournal();

    function resultMarkJournal() {
        let parent = document.querySelectorAll('.marks');
        parent.forEach(item => {
            let val = 0;
            for (var i=0; i < item.querySelectorAll('.journal__mark').length; i++) {
                if (item.querySelectorAll('.journal__mark')[i].childNodes[1].value) {
                    let newVar = +item.querySelectorAll('.journal__mark')[i].childNodes[1].value;
                    val += parseInt(newVar);
                }
            }
            item.querySelector('.journal__result').textContent = val;
        })
    }
    resultMarkJournal();

    function tapLink() {
        document.querySelectorAll('.menu__list a').forEach(item => {
            item.addEventListener('click', () => {
                localStorage.setItem('menuLink', item.href)
            })
        })
    }
    tapLink();

    function avatarChange() {
        let changeItem = document.querySelector('.change__edit');
        if (changeItem) {
            changeItem.onchange = function (evt) {
                var tgt = evt.target || window.event.srcElement,
                    files = tgt.files;

                // FileReader support
                if (FileReader && files && files.length) {
                    var fr = new FileReader();
                    fr.onload = function () {
                        document.querySelector('.change__photo').src = fr.result;
                        document.querySelector('.header__icon img').src = fr.result;
                    }
                    fr.readAsDataURL(files[0]);
                }
            }
        }
    }
    avatarChange();

    function menuMobile() {
        if (document.querySelector(".header__menu")) {
            document.querySelector(".header__menu").addEventListener('click', () => {
                document.querySelector(".menu").style.right = "0";
            });
            document.querySelector(".menu__close").addEventListener('click', () => {
                document.querySelector(".menu").style.right = "-100%";
            });
        }
    }
    menuMobile();

    jQuery(document).ready(function() {
        jQuery(".journal-table").clone(true).appendTo('#journal-scroll').addClass('clone');
        jQuery(".shedule-table").clone(true).appendTo('#shedule-scroll').addClass('clone');
    });

    (function ($) {
        "use strict";

        $('.register__input').each(function(){
            $(this).on('blur', function(){
                if($(this).val().trim() != "") {
                    $(this).addClass('has-val');
                }
                else {
                    $(this).removeClass('has-val');
                }
            })
        });

function navOptShow() {
    let image = document.querySelector('.header__icon img'),
        navBlock = document.querySelector('.header__option'),
        navBlockItem = document.querySelector('.header__option--item');
    if (image) {
        image.addEventListener('click', () => {
            if (navBlock.classList.contains('navAdd')) {
                navBlock.style.cssText = "z-index: -2; opacity: 0; visibility: hidden";
                navBlock.classList.remove('navAdd');
            } else {
                navBlock.style.cssText = "z-index: 10; opacity: 1; visibility: visible";
                navBlock.classList.add('navAdd');
            }
        })
        document.body.addEventListener('click', (e) => {
            if (e.target !=image && e.target !=navBlock && e.target !=navBlockItem) {
                navBlock.style.cssText = "z-index: -2; opacity: 0; visibility: hidden";
                navBlock.classList.remove('navAdd');
            }
        });
    }

}
navOptShow();

function addNewSubject() {
    let modal = document.querySelector('.shedule__modal'),
        btn = document.querySelectorAll('.shedule__subject--mini'),
        close = document.querySelector('.shedule__modal--close')
    if (document.querySelectorAll('.shedule__subject--mini').length > 1) {
        btn.forEach((item) => {
            item.addEventListener('click', () => {
                document.querySelector("input[name='days']").value = item.dataset.day;
                document.querySelector("input[name='time']").value = item.dataset.time;
                modal.classList.add("subjectModalOpen")
            })
        })
        if (close) {
            close.addEventListener('click', (e) => {
                modal.classList.remove("subjectModalOpen")
            })
        }
    }

}
addNewSubject();

})(jQuery);

    jQuery(function($){
        $("#tel").mask("+7(999) 999-9999");
    });

    $('img.img-svg').each(function(){
        var $img = $(this);
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function(data) {
            var $svg = $(data).find('svg');
            if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $img.replaceWith($svg);
        }, 'xml');
    });



});
