function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function resultMarkJournal() {
    let parent = document.querySelectorAll('.marks');
    parent.forEach(item => {
        let val = 0;
        for (var i=0; i < item.querySelectorAll('.journal__mark').length; i++) {
            if (item.querySelectorAll('.journal__mark')[i].childNodes[1].value) {
                let newVar = +item.querySelectorAll('.journal__mark')[i].childNodes[1].value;
                val += parseInt(newVar);
            }
        }
        item.querySelector('.journal__result').textContent = val;
    })
}

function sendJournalMarks() {
    document.querySelectorAll('.journal__mark input').forEach(item => {
        item.addEventListener('change', (e) => {
            const request = new XMLHttpRequest();
            const url = e.target.dataset.action;
            const csrftoken = getCookie('csrftoken');
            const params = "csrfmiddlewaretoken=" + csrftoken + "&points=" +e.target.value;
            console.log(params)
            //	Здесь нужно указать в каком формате мы будем принимать данные вот и все	отличие
            request.responseType =	"json";
            request.open("POST", url, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            request.addEventListener("readystatechange", () => {

                if (request.readyState === 4 && request.status === 200) {
                    let obj = request.response;
                    resultMarkJournal();
                }
            });

            request.send(params);
        })
    })
}
sendJournalMarks();