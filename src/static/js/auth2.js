window.addEventListener('DOMContentLoaded', () => {

    $('img.img-svg').each(function(){
        var $img = $(this);
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        $.get(imgURL, function(data) {
            var $svg = $(data).find('svg');
            if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $img.replaceWith($svg);
        }, 'xml');
    });

    let square = document.querySelector('.cap__square'),
        image = document.querySelector('.cap__img'),
        pazlBlur = document.querySelector('.cap__white'),
        pazlBlurMobile = document.querySelector('.cap__whiteMobile'),
        loadIcon = document.querySelector('.cap__image--block'),
        unplashImgWidth = '',
        unplashImgHeight = '',
        pazl = document.querySelector('.cap__include'),
        pazlMobile = document.querySelector('.cap__includeMobile'),
        falseCheckCounter = 0,
        canvas = document.getElementById('myCanvas'),
        canvasMobile = document.getElementById('canvasPazlMobile'),
        context = canvas.getContext('2d'),
        contextMobile = canvasMobile.getContext('2d'),
        imageObj = new Image(),
        canvasBlur = document.getElementById('myCanvas1'),
        canvasBlurMobile = document.getElementById('canvasBlurMobile'),
        contextBlur = canvasBlurMobile.getContext('2d'),
        contextBlurMobile = canvasBlurMobile.getContext('2d'),
        mobileBlockWidth = document.querySelector('.cap__container'),
        slideBlock = document.querySelector('.cap__block'),
        captcha = document.querySelector('.captcha'),
        imageBehind = new Image(),
        attemptLeft = 3,
        pageX,
        sourceX,
        sourceY,
        fetchLinkFirst = "https://api.unsplash.com/photos/random?query=origami&orientation=landscape&count=1&client_id=PKKmuAc38DQj8Z7JwPiBoOFrk_5vXpEPXzwx_p8g98g",
        fetchLinkSecond = "https://api.unsplash.com/photos/random?query=origami&orientation=landscape&count=1&client_id=IEA2iAY7XgEYV1V5M7e4XUpDXbSQJKzy5Qkm51LQg3g";
        // PKKmuAc38DQj8Z7JwPiBoOFrk_5vXpEPXzwx_p8g98g
        // IEA2iAY7XgEYV1V5M7e4XUpDXbSQJKzy5Qkm51LQg3g

    function refreshChangeItem() {
        falseCheckCounter = 0;
        document.querySelector('.cap__alert').style.display = "none";
        attemptLeft = 3;
        pazl.style.left = "0px";
        pazlMobile.style.left = "0px";
        square.style.left = "0px";
        document.querySelector('.cap__arrow').style.cssText = "display: block";
        document.querySelector('.cap__no').style.cssText = "display: none";
        document.querySelector('.cap__check').style.cssText = "display: none";
        square.style.background = "#707ad6";
    }

    // Функция капчи на десктоп расширении
    function CaptchaImage() {
        // Получение случайного изображения пр api и установка его на фон капчи
        function loadImg() {
            fetch(fetchLinkFirst)
                .then(function(response){
                    return response.json();
                })
                .then(function(data){
                    image.src = data[0].urls.small;
                })
                .catch(function (e) {
                    console.log(e)
                    fetch(fetchLinkSecond)
                        .then(function(response){
                            return response.json();
                        })
                        .then(function(data){
                            image.src = data[0].urls.small;
                        })
                        .catch(function (e) {
                            console.log(e); // Вывод сообщения об ошибке в консоль
                        }) // Вывод сообщения об ошибке в консоль
                })
        }  
        loadImg();
        
    
        $('.cap__img').on('load', function(){ 
            captcha.style.cssText = "opacity: 1; z-index: 20; visibility: visible";
            // Получение ширины и высоты полученной с api картинки
            unplashImgWidth = this.width; 
            unplashImgHeight = this.height;
            // Привязка ползунка к высоте картинки
            square.style.top = unplashImgHeight + 11 +'px';

            // Псевдослучайное определение позиции пазла отностительно верхней и левой границы
            let sourceX = Math.floor(Math.random() * unplashImgWidth),
                squareWidth = square.clientWidth,
                squareHeight = square.clientHeight,
                sourceY = Math.floor(Math.random() * unplashImgHeight);
            
            if (sourceX > unplashImgWidth-squareWidth) {
                sourceX = unplashImgWidth-squareWidth
            } else if (sourceX < squareWidth) {
                sourceX = 100
            }
            sourceY = sourceY > unplashImgHeight-squareHeight ? sourceY = unplashImgHeight-squareHeight : sourceY;

            // Установка координат пазла и его конечного места назначения относительно верхней и левой границы
            pazl.style.top = sourceY+'px';
            pazlBlur.style.top = sourceY+'px';
            pazlBlur.style.left = sourceX+'px';

            // Обрезка изображения под квадрат размером 50 на 50 пикселей
            imageObj.onload = function newImageObj() {
                var sourceWidth = 50;
                var sourceHeight = 50;
                var destWidth = sourceWidth;
                var destHeight = sourceHeight;
                var destX = canvas.width / 2 - destWidth / 2;
                var destY = (canvas.height / 2 - destHeight / 2);
                context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
            };
            imageObj.src = image.getAttribute('src');
            
            // Создание квадрата с полпрозрачным белым фоном
            imageBehind.onload = function() {
                var sourceWidth = 50;
                var sourceHeight = 50;
                var destWidth = sourceWidth;
                var destHeight = sourceHeight;
                var destX = canvasBlur.width / 2 - destWidth / 2;
                var destY = canvasBlur.height / 2 - destHeight / 2;
                contextBlur.drawImage(imageBehind, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
            };

            // Функция валидации
            function validation() {
            
                square.style.position = 'absolute';
                square.style.zIndex = '1000';
                let block = document.querySelector('.cap__sign');

                function moveAt(pageX, pageY) {
                    document.querySelector('.cap__arrow').style.cssText = "display: block";
                    document.querySelector('.cap__check').style.cssText = "display: none";
                    document.querySelector('.cap__no').style.cssText = "display: none";
                    let squarePos = pageX - block.getBoundingClientRect().left - square.clientWidth / 2;
                    if (squarePos > -1 && squarePos < slideBlock.clientWidth+2-square.clientWidth) {
                        pazl.style.left = pageX - block.getBoundingClientRect().left - square.clientWidth / 2  + 'px';
                        square.style.left = pageX - block.getBoundingClientRect().left - square.clientWidth / 2  + 'px';
                        square.style.top = unplashImgHeight + 11 +'px';
                    } else {
                        square.onmouseup = function() {
                            document.removeEventListener('mousemove', onMouseMove);
                            square.onmouseup = null;
                        };
                    }
                    if (squarePos > 5) {
                        document.querySelector('.cap__text').style.cssText = "opacity: 0"
                    } else {
                        document.querySelector('.cap__text').style.cssText = "opacity: 1"
                    }
                    square.style.background = "#707ad6";
                }
                function onMouseMove(event) {
                    moveAt(event.pageX, event.pageY)
                }
                
                // Перемещение ползунка
                document.addEventListener('mousemove', onMouseMove);
                // Прекращение перемещения ползунка по экрану и проверка результата
                document.body.onmouseup = function(e) {
                    document.removeEventListener('mousemove', onMouseMove);
                    
                    let squarePos = square.getBoundingClientRect().left - block.getBoundingClientRect().left;
                    if (squarePos > sourceX - 6 && squarePos < sourceX + 6) {
                        square.style.background = "#57b846";
                        document.querySelector('.cap__arrow').style.cssText = "display: none";
                        document.querySelector('.cap__check').style.cssText = "display: block";
                        let form = document.querySelector('.register__form');
                        $('form').serialize();
                        form.submit();
                    } else {
                        falseCheckCounter++;
                        document.querySelector('.cap__alert').style.display = "block";
                        document.querySelector('.cap__alert--num').textContent = attemptLeft;
                        attemptLeft--;
                        square.style.background = "#e53030";
                        document.querySelector('.cap__arrow').style.cssText = "display: none"
                        document.querySelector('.cap__no').style.cssText = "display: block"
                    }
                    // Если больше трех неправильных попыток, картинка меняется
                    if (falseCheckCounter > 3) {
                        loadImg();
                        falseCheckCounter = 0;
                        document.querySelector('.cap__alert').style.display = "none";
                        attemptLeft = 3;
                        pazl.style.left = "0px";
                        square.style.left = "0px";
                        document.querySelector('.cap__arrow').style.cssText = "display: block";
                        document.querySelector('.cap__no').style.cssText = "display: none";
                        document.querySelector('.cap__check').style.cssText = "display: none";
                        square.style.background = "#707ad6";
                    }
                };
                
            }

            // Запуск функции валидации при отжатии ползунка
            square.onmousedown = validation;
            pazl.onmousedown = validation;
            square.ondragstart = function() {
                return false;
            }
            pazl.ondragstart = function() {
                return false;
            }
            
        });
        // Клик по кнопке смены изображения
        document.querySelector('.cap__refresh').addEventListener('click', () => {
            loadIcon.style.display = "block";
            fetch(fetchLinkFirst)
                .then(function(response){
                    return response.json();
                })
                .then(function(data){
                    image.src = data[0].urls.small;
                    loadIcon.style.display = "none";
                })
                .catch(function (e) {
                    console.log(e); // что-то пошло не так
                    fetch(fetchLinkSecond)
                        .then(function(response){
                            return response.json();
                        })
                        .then(function(data){
                            image.src = data[0].urls.small;
                            loadIcon.style.display = "none";
                        })
                        .catch(function (e) {
                            console.log(e); // что-то пошло не так
                        })
                })
            refreshChangeItem();
        });

        // Клик по кнопке закрытия капчи
        document.querySelector('.cap__close').addEventListener('click', () => {
            captcha.style.cssText = "opacity: 0; z-index: -10; visibility: hidden";
        })
    }
   
    // Функция капчи на мобильном устройстве
    function CaptchaImageMobile() {
        // Получение случайного изображения пр api и установка его на фон капчи
        function loadImg() {
            fetch(fetchLinkFirst)
                .then(function(response){
                    return response.json();
                })
                .then(function(data){
                    let link = data[0].urls.small;
                    let n = link.substr(link.indexOf("w=")+ 2);
                    let w = mobileBlockWidth.clientWidth;
                    let newLink = link.replace(n , w);
                    image.src = newLink;
                    
                })
                .catch(function (e) {
                    console.log(e); // Вывод сообщения об ошибке в консоль
                    fetch(fetchLinkSecond)
                        .then(function(response){
                            return response.json();
                        })
                        .then(function(data){
                            let link = data[0].urls.small;
                            let n = link.substr(link.indexOf("w=")+ 2);
                            let w = mobileBlockWidth.clientWidth;
                            let newLink = link.replace(n , w);
                            image.src = newLink;

                        })
                        .catch(function (e) {
                            console.log(e); // Вывод сообщения об ошибке в консоль
                        })
                })
        }  
        loadImg();
        
    
        $('.cap__img').on('load', function(){ 
            captcha.style.cssText = "opacity: 1; z-index: 20; visibility: visible";
            // Получение ширины и высоты полученной с api картинки
            unplashImgWidth = this.width; 
            unplashImgHeight = this.height;
            // Привязка ползунка к высоте картинки
            square.style.top = unplashImgHeight + 11 +'px';

            let sourceX = Math.floor(Math.random() * unplashImgWidth),
                squareWidth = square.clientWidth,
                squareHeight = square.clientHeight,
                sourceY = Math.floor(Math.random() * unplashImgHeight);
            if (sourceX > unplashImgWidth-squareWidth) {
                sourceX = unplashImgWidth-squareWidth
            } else if (sourceX < squareWidth+30) {
                sourceX = 70
            }
            sourceY = sourceY > unplashImgHeight-squareHeight ? sourceY = unplashImgHeight-squareHeight : sourceY;

            // Установка координат пазла и его конечного места назначения относительно верхней и левой границы
            pazlMobile.style.top = sourceY+'px';
            pazlBlurMobile.style.top = sourceY+'px';
            pazlBlurMobile.style.left = sourceX+'px';

            // Обрезка изображения под квадрат размером 50 на 50 пикселей
            imageObj.onload = function newImageObj() {
                var sourceWidth = 40;
                var sourceHeight = 40;
                var destWidth = sourceWidth;
                var destHeight = sourceHeight;
                var destX = canvasMobile.width / 2 - destWidth / 2;
                var destY = (canvasMobile.height / 2 - destHeight / 2);
                contextMobile.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
            };
            imageObj.src = image.getAttribute('src');
            
            // Создание квадрата с полпрозрачным белым фоном
            imageBehind.onload = function() {
                var sourceWidth = 40;
                var sourceHeight = 40;
                var destWidth = sourceWidth;
                var destHeight = sourceHeight;
                var destX = canvasBlurMobile.width / 2 - destWidth / 2;
                var destY = canvasBlurMobile.height / 2 - destHeight / 2;
                contextBlurMobile.drawImage(imageBehind, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
            };
        });
        // Передвижение ползунка
        function moveTouch(event) {
            event.preventDefault();
            square.style.position = 'absolute';
            square.style.zIndex = '1000';
            let block = document.querySelector('.cap__sign');
            document.querySelector('.cap__arrow').style.cssText = "display: block";
            document.querySelector('.cap__check').style.cssText = "display: none";
            document.querySelector('.cap__no').style.cssText = "display: none";
            pageX = event.changedTouches[0].clientX - block.getBoundingClientRect().left - square.clientWidth;
            let pageXRight = event.changedTouches[0].clientX - block.getBoundingClientRect().left - 2;
            
            if (pageX > -1 && pageXRight < slideBlock.clientWidth) {
                pazlMobile.style.left = pageX + 'px';
                square.style.left = pageX + 'px';
                square.style.top = unplashImgHeight + 11 +'px';
            } 
            if (pageX > 10) {
                document.querySelector('.cap__text').style.cssText = "opacity: 0";
            } else {
                document.querySelector('.cap__text').style.cssText = "opacity: 1";
            }
            square.style.background = "#707ad6";
        }

        // Окончание нажатия на экран
        function endTouch(e) {
            e.preventDefault();
            e.target.removeEventListener("touchmove", moveTouch);
            e.target.removeEventListener("touchend",  endTouch);
            if (pageX > sourceX - 6 && pageX < sourceX + 6) {
                square.style.background = "#57b846";
                document.querySelector('.cap__arrow').style.cssText = "display: none";
                document.querySelector('.cap__check').style.cssText = "display: block";
                let form = document.querySelector('.register__form');
                $('form').serialize();
                form.submit();
            } else {
                falseCheckCounter++;
                document.querySelector('.cap__alert').style.display = "block";
                document.querySelector('.cap__alert--num').textContent = attemptLeft;
                attemptLeft--;
                square.style.background = "#e53030";
                document.querySelector('.cap__arrow').style.cssText = "display: none"
                document.querySelector('.cap__no').style.cssText = "display: block";
                // Если больше трех неправильных попыток, картинка меняется
                if (falseCheckCounter > 3) {
                    falseCheckCounter = 0;
                    loadImg();
                    document.querySelector('.cap__alert').style.display = "none";
                    attemptLeft = 3;
                    pazlMobile.style.left = "0px";
                    square.style.left = "0px";
                    document.querySelector('.cap__arrow').style.cssText = "display: block";
                    document.querySelector('.cap__no').style.cssText = "display: none";
                    document.querySelector('.cap__check').style.cssText = "display: none";
                    square.style.background = "#707ad6";
                }
            }
        }

        // Начало клика на элемент
        square.addEventListener('touchstart', (e) => {
            e.preventDefault();
            e.target.addEventListener('touchmove', moveTouch);
            e.target.addEventListener('touchend', endTouch);
        }, {passive: false})

        // Клик по кнопке смены изображения
        document.querySelector('.cap__refresh').addEventListener('click', () => {
            loadIcon.style.display = "block";
            fetch(fetchLinkFirst)
                .then(function(response){
                    return response.json();
                })
                .then(function(data){
                    let link = data[0].urls.small;
                    let n = link.substr(link.indexOf("w=")+ 2);
                    let w = mobileBlockWidth.clientWidth;
                    let newLink = link.replace(n , w);
                    image.src = newLink;
                    loadIcon.style.display = "none";
                })
                .catch(function (e) {
                    console.log(e); // что-то пошло не так
                    fetch(fetchLinkSecond)
                        .then(function(response){
                            return response.json();
                        })
                        .then(function(data){
                            let link = data[0].urls.small;
                            let n = link.substr(link.indexOf("w=")+ 2);
                            let w = mobileBlockWidth.clientWidth;
                            let newLink = link.replace(n , w);
                            image.src = newLink;
                            loadIcon.style.display = "none";
                        })
                        .catch(function (e) {
                            console.log(e); // что-то пошло не так
                        })
                })
            refreshChangeItem();
        });

        // Клик по кнопке закрытия капчи
        document.querySelector('.cap__close').addEventListener('click', () => {
            captcha.style.cssText = "opacity: 0; z-index: -10; visibility: hidden";
        })
        
    }

    let enterBtn = document.querySelector('.register__btn');
    if (enterBtn) {
        enterBtn.addEventListener('click', (e) => {
            e.preventDefault()
            if (window.innerWidth > 460) {
                CaptchaImage();
            } else {
                CaptchaImageMobile();
            }
        });
    };

(function ($) {
    $('.register__input').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
})(jQuery);
    
})