import uuid

def generate_uuid():
    result = uuid.uuid4()
    return result


def upd_res_by_dict_key(list, key, elem):
    for i in range(len(list)):
        if isinstance(list[i], dict):
            if key in list[i].keys():
                list[i][key].append(elem)
                return {'has_dict': True, 'result': list}
    return {'has_dict': False}


def sort_by_days(x):
    if isinstance(x, dict):
        if isinstance(x[next(iter(x))], list):
            return int(next(iter(x)))
        else:
            return x['days']
    else:
        return int(x.days)


def normalize_result(result):
    for lessons in result:
        sort_numbers = []
        for lesson in lessons:
            if lesson:
                if isinstance(lesson, dict):
                    sort_numbers.append(next(iter(lesson)))
                else:
                    sort_numbers.append(lesson.days)
        for i in range(5):
            if len(lessons) < 5 and i not in sort_numbers:
                lessons.append({'days': i, 'empty': True})
        lessons.sort(key=sort_by_days)
        # temp_lessons = []
        # for i in sort_numbers:
        #     temp_lessons.append(lessons[i])
        # print(temp_lessons)
    print(lessons)

    return result


def get_journal_info(all_lessons):
    result = []
    times = []
    for _ in range(5):
        result.append([])
        times.append([])

    for lesson in all_lessons:
        if lesson.mode != 0:
            temp_result = upd_res_by_dict_key(result[lesson.time - 1], lesson.days, lesson)
            if temp_result['has_dict']:
                print('Yes')
            else:
                result[lesson.time - 1].append({lesson.days: [lesson]})
        else:
            result[lesson.time - 1].append(lesson)

    result = normalize_result(result)
    print(result)
    return result
