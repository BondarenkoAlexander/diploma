from functools import partial

from field_permissions.models import FieldPermissionModelMixin


class FieldPermissionModelMixinV3(FieldPermissionModelMixin):
    FIELD_PERM_CODENAME_APP = '{app_label}.can_change_{model}_{name}'

    def has_field_perm(self, user, field):
        if field in self.field_permissions:
            checks = self.field_permissions[field]
            if not isinstance(checks, (list, tuple)):
                checks = [checks]
            for i, perm in enumerate(checks):
                if callable(perm):
                    checks[i] = partial(perm, field=field)

        else:
            checks = []

            # Consult the optional field-specific hook.
            getter_name = self.FIELD_PERMISSION_GETTER.format(name=field)
            if hasattr(self, getter_name):
                checks.append(getattr(self, getter_name))

            # Try to find a static permission for the field
            else:
                perm_label = self.FIELD_PERM_CODENAME.format(**{
                    'model': self._meta.model_name,
                    'name': field,
                })
                perm_label_app = self.FIELD_PERM_CODENAME_APP.format(**{
                    'app_label': self._meta.app_label,
                    'model': self._meta.model_name,
                    'name': field,
                })

                if perm_label in dict(self._meta.permissions):
                    checks.append(perm_label_app)

        # No requirements means no restrictions.
        if not len(checks):
            return self.FIELD_PERMISSION_MISSING_DEFAULT

        # Try to find a user setting that qualifies them for permission.
        for perm in checks:
            if callable(perm):
                result = perm(self, user=user)
                if result is not None:
                    return result
            else:
                result = user.has_perm(perm)  # Don't supply 'obj', or else infinite recursion.
                if result:
                    return True

        # If no requirement can be met, then permission is denied.
        return False
