from django.urls import path

from consultations.views import ConsultationListView

app_name = 'consultations'

urlpatterns = [
    path('', ConsultationListView.as_view(), name='list'),
]