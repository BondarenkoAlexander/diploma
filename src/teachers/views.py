from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from accounts import roles
from teachers.forms import TeacherCreateForm, TeacherUpdateForm
from teachers.models import Teacher


class TeachersListView(LoginRequiredMixin, ListView, PermissionRequiredMixin):
    model = Teacher
    template_name = 'teachers/teachers-list.html'
    context_object_name = 'teachers'

    def get_object(self):
        teachers = Teacher.objects.all().values('user__id', 'user__first_name', 'user__last_name', 'user__surname',
                                                'user__email',
                                                'user__phone_number', 'academic_title','academic_degree')
        return teachers

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teachers'] = self.get_object()
        context['role_id'] = roles.TEACHERS.id

        return context

    # def get(self, request, *args, **kwargs):
    #     teachers = Teacher.objects.all().values('user__first_name', 'user__last_name', 'user__surname', 'user__email',
    #                                             'user__phone_number')
    #
    #     return render(request,
    #                   template_name='teachers/teachers-list.html')


class TeacherCreateView(LoginRequiredMixin, CreateView, PermissionRequiredMixin):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers/teacher-create.html'
    success_url = reverse_lazy('index')


class TeacherUpdateView(LoginRequiredMixin, UpdateView, PermissionRequiredMixin):
    model = Teacher
    form_class = TeacherUpdateForm
    template_name = 'teachers/teacher-update.html'
    success_url = reverse_lazy('index')


class TeacherDeleteView(LoginRequiredMixin, DeleteView, PermissionRequiredMixin):
    model = Teacher
    template_name = 'teachers/teacher-delete.html'
    success_url = reverse_lazy('index')
    pk_url_kwarg = 'teacher_id'

