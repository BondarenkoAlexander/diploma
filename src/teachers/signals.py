from django.db.models.signals import pre_delete
from django.dispatch import receiver

from teachers.models import Teacher


@receiver(pre_delete, sender=Teacher)
def delete_student(sender, instance, **kwargs):
    instance.user.is_teacher=False
    instance.user.save()