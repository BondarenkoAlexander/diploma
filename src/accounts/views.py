import json

from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordResetView, \
    PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView
from django.urls import reverse

from accounts.forms import AccountRegistrationForm, AccountLoginForm, ResetPasswordForm, \
    ResetPasswordConfirmForm, AccountAjaxUpdateForm, AccountUpdateProfileForm, AccountsChangePassword
from accounts.models import User


class AccountListView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    permission_required = 'lessons.create_mark'
    model = User

    def get(self, request, *args, **kwargs):
        if request.is_ajax():

            q = request.GET.get('term', '').capitalize()
            filter_set = Q()
            for item in q.split(' '):
                filter_set |= Q(first_name__icontains=item) | Q(last_name__icontains=item) | Q(surname__icontains=item)
            search_qs = User.objects.filter(filter_set, is_teacher = False, is_student = False, is_staff = False)[:6]
            # is_teacher = False, is_student = False, is_staff = False

            results = {}
            for i in range(len(search_qs)):
                results.update({f'{i}': {'full_name': search_qs[i].get_full_name(), 'id': search_qs[i].id}})

            data = json.dumps(results)
            mimetype = 'application/json'
            return HttpResponse(data, mimetype)


class AccountRegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    success_url = reverse_lazy('accounts:login')
    form_class = AccountRegistrationForm


class AccountLoginView(LoginView):
    template_name = 'login.html'
    form_class = AccountLoginForm

    def form_valid(self, form):
        remember = self.request.POST.get('remember-me', False)
        if not remember:
            self.request.session.set_expiry(0)
        return super().form_valid(form)


class AccountLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'logout.html'
    success_url = reverse_lazy('accounts:login')

    def get_redirect_url(self):
        return reverse('accounts:login')


class AccountUpdateUserView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'lessons.create_mark'
    model = User
    pk_url_kwarg = 'user_id'
    form_class = AccountAjaxUpdateForm
    template_name = 'account-update.html'
    success_url = reverse_lazy('index')

    def post(self, request, user_id, *args, **kwargs):
        print(request.POST)
        user = User.objects.get(id=user_id)

        form = AccountAjaxUpdateForm(instance=user,
                                     data=request.POST)
        if form.is_valid():
            form.save()
        else:
            print(form.errors)
            return HttpResponse(status=500)

        data = {'name': user.get_full_name(), 'phone_number': user.phone_number, 'academic_degree': 'Доцент',
                'email': user.email, 'id': user.id}

        return JsonResponse(data)


class AccountUpdateView(LoginRequiredMixin,UpdateView):
    model = User
    template_name = 'profile.html'
    success_url = reverse_lazy('accounts:profile')
    form_class = AccountUpdateProfileForm

    def form_valid(self, form):
        form.cleaned_data['image'] = self.request.FILES.get('image')
        form.cleaned_data.update({'academic_title':self.request.POST.get('academic_title', '')})
        form.cleaned_data.update({'academic_degree':self.request.POST.get('academic_degree', '')})
        form.save()
        return super().form_valid(form)

    def get_object(self, queryset=None):
        return self.request.user


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password-change.html'
    success_url = reverse_lazy('index')
    form_class = AccountsChangePassword


class AccountPasswordResetView(PasswordResetView):
    email_template_name = 'password_reset_email.html'
    html_email_template_name = 'password_reset_email.html'
    template_name = 'reset_password.html'
    success_url = reverse_lazy('accounts:password_reset_done')
    form_class = ResetPasswordForm


class AccountPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'password_reset_sent.html'
    success_url = reverse_lazy('accounts:password_reset_confirm')


class AccountPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'password_reset_form.html'
    success_url = reverse_lazy('index')
    form_class = ResetPasswordConfirmForm


class AccountPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'password_reset_form.html'
    success_url = reverse_lazy('accounts:profile')
