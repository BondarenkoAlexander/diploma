from django.contrib.auth.forms import UserChangeForm, UserCreationForm, AuthenticationForm, PasswordResetForm, \
    SetPasswordForm, PasswordChangeForm
from django.forms import ModelForm

from accounts.models import User
from accounts.utils import change_auth_group


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'email', 'password1', 'password2', 'last_name', 'first_name']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'register__input',
            })


class AccountsChangePassword(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'reset__input reset__input--new',
                'required': 'True'
            })
        self.fields['old_password'].widget.attrs.update({
            'placeholder': 'Старий пароль',
        })
        self.fields['new_password1'].widget.attrs.update({
            'placeholder': 'Новий пароль',
        })
        self.fields['new_password2'].widget.attrs.update({
            'placeholder': 'Підтвердіть новий пароль',
        })


class UserChangeAdminForm(ModelForm):
    class Meta:
        fields = '__all__'

    def save(self, commit=True):
        user_role = self.cleaned_data['groups']
        change_auth_group(self.instance, user_role)
        return super().save(commit=False)


class AccountLoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'register__input'
            })


class AccountUpdateForm(UserChangeForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['surname', 'first_name', 'last_name', 'phone_number', 'username', 'email', 'image', 'is_teacher',
                  'is_student', 'groups']
        # fields = '__all__'

    def save(self, commit=True):
        user_role = self.cleaned_data['groups']
        change_auth_group(self.instance, user_role)
        return super().save(commit=True)


class AccountUpdateProfileForm(UserChangeForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['surname', 'first_name', 'last_name', 'phone_number', 'username', 'email', 'image']
        # fields = '__all__'

    def save(self, commit=True):
        if self.instance.is_teacher:
            self.instance.teacher.academic_title = self.cleaned_data['academic_title']
            self.instance.teacher.academic_degree = self.cleaned_data['academic_degree']
            self.instance.teacher.save()

        return super().save(commit=True)


class AccountAjaxUpdateForm(UserChangeForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['is_teacher', 'is_student', 'groups']
        # fields = '__all__'

    def clean(self):
        cleaned_data = super(AccountAjaxUpdateForm, self).clean()
        cleaned_data['group_uuid'] = self.data.get('group_uuid', '')
        return cleaned_data

    def save(self, commit=False):
        user_role = self.cleaned_data['groups']
        user_group_uuid = self.cleaned_data.get('group_uuid', '')
        change_auth_group(self.instance, user_role, user_group_uuid=user_group_uuid)

        return super().save(commit=True)


class ResetPasswordForm(PasswordResetForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({
            'class': 'reset__input',
            'required': 'True',
            'placeholder': 'E-mail',
        })


class ResetPasswordConfirmForm(SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'reset__input reset__input--new',
                'required': 'True'
            })
        self.fields['new_password1'].widget.attrs.update({
            'placeholder': 'Новий пароль',
        })
        self.fields['new_password2'].widget.attrs.update({
            'placeholder': 'Підтвердіть новий пароль',
        })
