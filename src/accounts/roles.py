from django.contrib.auth.models import Group

STUDENTS = Group.objects.get(name='Students')
TEACHERS = Group.objects.get(name='Teachers')
