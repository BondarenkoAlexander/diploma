from django.urls import path
from accounts.views import AccountRegistrationView, AccountLoginView, AccountLogoutView, AccountUpdateView, \
    AccountPasswordChangeView, AccountPasswordResetView, AccountPasswordResetDoneView, AccountPasswordResetConfirmView, \
    AccountPasswordResetCompleteView, AccountListView, AccountUpdateUserView

app_name = "accounts"

urlpatterns = [
    path('registration/', AccountRegistrationView.as_view(), name='registration'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('profile/', AccountUpdateView.as_view(), name='profile'),
    path('update/<int:user_id>', AccountUpdateUserView.as_view(), name='update'),
    path('password/', AccountPasswordChangeView.as_view(), name='password_change'),
    path('reset_password/', AccountPasswordResetView.as_view(),
         name='reset_password'),
    path('reset_password_sent/', AccountPasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>', AccountPasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset_password_complete/', AccountPasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
    path('list/', AccountListView.as_view(), name='list'),

]
