from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from lessons.models import Mark
from students.models import Student



@receiver(pre_delete, sender=Student)
def delete_student(sender, instance, **kwargs):
    Mark.objects.filter(student=instance.user).delete()
    instance.user.is_student=False
    instance.user.save()


@receiver(post_save, sender=Student)
def save_student(sender, instance, **kwargs):
    Mark.objects.filter(student=instance.user).delete()
    for subject in instance.group.subjects.all():
        for lesson in subject.get_all_lessons():
            if lesson:
                for week in range(1, instance.group.semester_weeks + 1):
                    obj = Mark(
                        lesson=lesson,
                        group=instance.group,
                        subject=subject,
                        teacher=lesson.teacher,
                        student=instance.user,
                        week=week,
                    )
                    if lesson.mode == 0:
                        obj.save()
                    elif lesson.mode == 1 and week % 2 == 0:
                        obj.save()
                    elif lesson.mode == 2 and week % 2 != 0:
                        obj.save()