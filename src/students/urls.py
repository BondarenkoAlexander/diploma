from django.urls import path

from students.views import StudentCreateView, StudentUpdateView, StudentDeleteView, StudentsListView

app_name = 'students'

urlpatterns = [
    path('create/', StudentCreateView.as_view(), name='create'),
    path('update/', StudentUpdateView.as_view(), name='update'),
    path('delete/<int:student_id>/', StudentDeleteView.as_view(), name='delete'),
    path('list/', StudentsListView.as_view(), name='list'),
]