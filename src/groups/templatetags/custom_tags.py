from django import template

from core.utils import generate_uuid
from lessons.models import Lesson

register = template.Library()


def negate(value):
    return -value


def mult(value, arg):
    return value * arg


def div(value, arg):
    return value / arg


def get_range(value, arg):
    return [i for i in range(arg)]


def get_time(value):
    time = Lesson.LESSON_TIME
    return time[value-1][1]


def get_time_value(value):
    time = Lesson.LESSON_TIME
    return time[value-1][0]


def get_dict_value(value):
    return value[next(iter(value))]


def gen_uuid(value):
    return generate_uuid()


register.filter('negate', negate)
register.filter('mult', mult)
register.filter('div', div)
register.filter('get_range', get_range)
register.filter('get_time', get_time)
register.filter('gen_uuid', gen_uuid)
register.filter('get_time_value', get_time_value)
register.filter('get_dict_value', get_dict_value)
