import datetime

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

# Create your models here.
from core.utils import generate_uuid
from lessons.models import Subject


class Group(models.Model):
    YEAR_CHOICES = [(r, r) for r in range(1918, datetime.date.today().year)]

    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    name = models.CharField(null=False, max_length=240)
    start_year = models.IntegerField(choices=YEAR_CHOICES)
    number = models.IntegerField(validators=[MinValueValidator(1), ], null=True, default=1)
    semester_weeks = models.IntegerField(validators=[MinValueValidator(10), MaxValueValidator(16)], default=15)

    subjects = models.ManyToManyField(
        to=Subject,
        related_name='groups',
    )

    headman = models.OneToOneField(
        to='accounts.User',
        on_delete=models.SET_NULL,
        null=True,
        related_name='headed_group'
    )

    def __str__(self):
        short_name = ""
        for temp_name in self.name.split(' '):
            short_name += temp_name[:1].upper()
        return f'{short_name}-{str(self.start_year)[2:]}{self.number}'
