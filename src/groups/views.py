from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.views.generic import DetailView, UpdateView, ListView

from accounts import roles
from groups.forms import GroupUpdateForm
from core.utils import get_journal_info
from groups.models import Group
from lessons.forms import LessonCreateForm
from lessons.models import Lesson, Mark
from lessons.utils import get_weeks_colspan


class StudentGroupView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    permission_required = 'lessons.view_mark'
    model = Group
    pk_url_kwarg = 'group_uuid'
    template_name = 'groups/group-detail.html'
    context_object_name = 'group'

    def get_object(self):
        uuid = self.kwargs.get('group_uuid')
        return self.model.objects.get(uuid=uuid)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.select_related('group')

        return context


class StudentListView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    permission_required = 'group.view_group'
    model = Group
    template_name = 'groups/groups-list.html'
    context_object_name = 'groups'


class AdminModerateGroup(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'group.change_group'
    # model = Group
    # template_name = 'groups/group-moderate.html'
    # pk_url_kwarg = 'group_uuid'
    # form_class = GroupUpdateForm
    # success_url = reverse_lazy('groups:moderate')
    #
    # def get_object(self):
    #     uuid = self.kwargs.get('group_uuid')
    #     return self.model.objects.get(uuid=uuid)

    def get(self, request, group_uuid):
        group = Group.objects.get(uuid=group_uuid)
        lessons = group.lessons_group.distinct().select_related('teacher', 'subject', 'group').order_by('time', 'days')

        lessons_journal = get_journal_info(lessons)

        group_form = GroupUpdateForm(instance=group)
        lesson_create_form = LessonCreateForm(group_uuid=group_uuid)


        return render(
            request=request,
            template_name='groups/group-moderate.html',
            context={
                'group_form': group_form,
                'lesson_create_form': lesson_create_form,
                'lessons': lessons_journal,
                'group': group,
            }
        )


class AdminModerateStudentsGroup(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    permission_required = 'group.change_group'

    def get(self, request, group_uuid, *args, **kwargs):
        group = Group.objects.get(uuid=group_uuid)
        students = group.students.all().values('user__id', 'user__first_name', 'user__last_name', 'user__surname',
                                               'user__email',
                                               'user__phone_number')
        role_id = roles.STUDENTS.id

        return render(
            request=request,
            template_name='groups/group-moderate-student.html',
            context={
                'students': students,
                'group': group,
                'role_id': role_id
            }
        )


class StudentJournalView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    permission_required = 'lessons.view_mark'

    def get(self, request, group_uuid):
        group = Group.objects.get(uuid=group_uuid)

        subjects = group.subjects.select_related('teacher')

        all_lessons = group.lessons_group.distinct().select_related('group', 'subject', 'teacher').order_by('time',
                                                                                                            'days')

        lessons = get_journal_info(all_lessons)

        return render(
            request=request,
            template_name='groups/journal.html',
            context={
                'group': group,
                'lessons': lessons,
                'subjects': subjects,
            }
        )


class SubjectStudentJournalView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'lessons.view_mark'

    def get(self, request, group_uuid, subject_uuid):
        group = Group.objects.get(uuid=group_uuid)
        groups = Group.objects.all()
        subject = group.subjects.get(uuid=subject_uuid)
        students = group.students.select_related('group').values('user', 'user__first_name', 'user__surname')
        lessons = group.lessons_group.filter(subject__uuid=subject_uuid).distinct()

        subjects = group.subjects.all()

        marks = list(Mark.objects.select_related('student', 'subject', 'group', 'lesson', 'teacher').filter(
            student__in=students.values('user'), subject__uuid=subject_uuid, group=group).order_by('lesson__mode',
                                                                                                   'week').values('id',
                                                                                                                  'points',
                                                                                                                  'student',
                                                                                                                  'lesson__mode'))

        colspan_weeks = get_weeks_colspan(lessons, group)

        return render(
            request=request,
            template_name='groups/journal-student-marks.html',
            context={
                'group': group,
                'groups': groups,
                'students': students,
                'lessons': lessons,
                'marks': marks,
                'subject': subject,
                'subjects': subjects,
                'colspan_weeks': colspan_weeks,

            }
        )


class JournalSelectView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'lessons.view_mark'

    def get(self, request):

        subjects = []

        if self.request.user.is_teacher:
            lessons = Lesson.objects.filter(teacher=self.request.user).distinct()
            group_list = self.request.user.teacher.learn_groups.all()
        if self.request.user.is_student:
            lessons = Lesson.objects.filter(group=self.request.user.student.group).distinct()
            group_list = self.request.user.student.group

        for lesson in lessons:
            if lesson.subject not in subjects:
                subjects.append(lesson.subject)

        return render(
            request=request,
            template_name='groups/select-journal.html',
            context={
                'lessons': lessons,
                'subjects': subjects,
                'group_list': group_list,
            }
        )


class TeacherJournalUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'lessons.change_mark'

    def get(self, request, group_uuid, subject_uuid):
        group = Group.objects.get(uuid=group_uuid)
        groups = self.request.user.teacher.learn_groups.all()
        students = group.students.select_related('group').values('user', 'user__first_name', 'user__last_name')
        lessons = group.lessons_group.filter(subject__uuid=subject_uuid, teacher=self.request.user).distinct()

        subjects = group.subjects.all().filter(lessons__teacher=self.request.user).distinct()

        marks = list(Mark.objects.select_related('student', 'subject', 'group', 'lesson', 'teacher').filter(
            student__in=students.values('user'), subject__uuid=subject_uuid, group=group,
            teacher=self.request.user).order_by('lesson__mode', 'week').values('id',
                                                                               'points',
                                                                               'student',
                                                                               'lesson__mode'))

        colspan_weeks = get_weeks_colspan(lessons, group)

        return render(
            request=request,
            template_name='groups/journal-teacher-marks.html',
            context={
                'group': group,
                'groups': groups,
                'students': students,
                'lessons': lessons,
                'marks': marks,
                'subjects': subjects,
                'colspan_weeks': colspan_weeks,
            }
        )
