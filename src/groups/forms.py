from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, ChoiceField

from accounts.models import User
from groups.models import Group
from lessons.models import Lesson

from collections import Counter


class LessonsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        days = Lesson.DAYS_OF_WEEK
        time = Lesson.LESSON_TIME
        lessons = []
        subjects = []
        unique_lesson = []
        for lesson in self.cleaned_data:
            if [lesson['days'], lesson['time'], lesson['subject'], lesson['group']] not in unique_lesson:
                unique_lesson.append([lesson['days'], lesson['time'], lesson['subject'], lesson['group']])
                lessons.append([lesson['days'], lesson['time']])
                subjects.append(lesson['subject'])

        count_lessons_in_week = Counter(subjects)
        for item in count_lessons_in_week:
            if count_lessons_in_week[item] > 2:
                raise ValidationError(f'Занадто багато занятть з {item} на тиждень')
        # raise ValidationError(f'Колізія часу занятть.Виправіть час занятть з предмету {subject} у {day[1]} {time[1]}')
        unique_lesson_by_time = []
        for lesson in lessons:
            if lesson not in unique_lesson_by_time:
                unique_lesson_by_time.append(lesson)
            else:
                raise ValidationError(f'Колізія часу занятть.Виправіть час занятть у {days[lesson[0]][1]} {time[lesson[1]-1][1]}')


class GroupBaseForm(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'


class GroupUpdateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        exclude = ['headman', 'uuid']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [(st.user.id, st.full_name()) for st in self.instance.students.all()]
        self.fields['headman_field'] = ChoiceField(choices=students)
        self.fields['headman_field'].initial = [0]

    def save(self, commit=True):
        headman_id = self.cleaned_data['headman_field']
        self.instance.headman = User.objects.get(id=headman_id)
        return super().save(commit=True)