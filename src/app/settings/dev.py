from app.settings.base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': os.environ['DB_NAME'],
#         'HOST': os.environ['DB_HOST'],
#         'USER': os.environ['DB_USER'],
#         'PORT': os.environ['DB_PORT'],
#         'PASSWORD': os.environ['DB_PASSWORD'],
#     }
# }
