from django.db import models

from core.utils import generate_uuid


class Subject(models.Model):
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    name = models.CharField(max_length=250, null=False)
    # group = models.ForeignKey(
    #     to='groups.Group',
    #     verbose_name="Дисципліни для групи",
    #     on_delete=models.CASCADE,
    #     related_name='subjects_group'
    # )

    def get_lessons(self):
        return self.lessons.filter(subject=self).first()

    def get_all_lessons(self):
        return self.lessons.filter(subject=self)

    def __str__(self):
        return f'{self.name}'


class Lesson(models.Model):
    TYPE_OF_LESSON = (
        ('0', 'Лекція'),
        ('1', 'Практика'),
        ('2', 'Лабораторна'),
        # ('3', 'РГР'),
        # ('4', 'Курсова робота'),
    )
    MODE_OF_LESSON = (
        (0, 'Звичайне заняття'),
        (1, 'Заняття по парним тижням'),
        (2, 'Заняття по не парним тижням')
    )
    DAYS_OF_WEEK = (
        (0, 'Понеділок'),
        (1, 'Вівторок'),
        (2, 'Середа'),
        (3, 'Четвер'),
        (4, "П'ятниця"),
    )
    LESSON_TIME = (
        (1, '8:00-9:35'),
        (2, '9:50-11:25'),
        (3, '11:40-13:15'),
        (4, '13:30-15:05'),
        (5, '15:20-16:55'),
    )
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    # date = models.DateField('Дата заняття', null=True)
    days = models.IntegerField(choices=DAYS_OF_WEEK, default=0)
    mode = models.IntegerField(choices=MODE_OF_LESSON, default=0)
    room = models.CharField(max_length=10, null=True)
    position = models.CharField(max_length=200, null=True)

    teacher = models.ForeignKey(
        to='accounts.User',
        null=True,
        verbose_name='Викладач',
        on_delete=models.CASCADE)

    subject = models.ForeignKey(
        to=Subject,
        verbose_name='Предмет',
        on_delete=models.CASCADE,
        related_name='lessons',
    )
    group = models.ForeignKey(
        to='groups.Group',
        verbose_name="Заняття для групи",
        on_delete=models.CASCADE,
        related_name='lessons_group'
    )
    time = models.IntegerField(choices=LESSON_TIME, default=1)
    write_date = models.DateField('Дата оновлення', auto_now=True)

    type = models.CharField(max_length=200, choices=TYPE_OF_LESSON, default='0')

    def get_type(self):
        return self.TYPE_OF_LESSON[int(self.type)][1]

    def get_short_type(self):
        return self.TYPE_OF_LESSON[int(self.type)][1][:3]

    def get_days(self):
        return self.DAYS_OF_WEEK[self.days][1]

    def get_time(self):
        return self.LESSON_TIME[self.time-1][1]

    def get_subject(self):
        return self.subject

    def get_full_mode(self):
        return self.MODE_OF_LESSON[self.mode][1]

    def get_mode(self):
        if self.mode == 1:
            return "Парн."
        elif self.mode == 2:
            return "Не парн."
        else:
            return "Звичайне заняття"

    def __str__(self):
        return f'{self.subject}.Занятие для группы {self.group}, {self.get_days()}, {self.get_type()}'


class Mark(models.Model):
    points = models.IntegerField('Оцінка', null=True)
    teacher_note = models.TextField('Коментар', null=True)
    updated = models.DateField('Оновлено', auto_now=True)
    week = models.SmallIntegerField('Тиждень заняття')

    lesson = models.ForeignKey(
        to=Lesson,
        verbose_name='Заняття',
        on_delete=models.CASCADE,
        related_name='mark_lesson',
        null=True
    )

    group = models.ForeignKey(
        to='groups.Group',
        verbose_name='Група',
        on_delete=models.CASCADE,
        related_name='mark_group',
        null=True,
    )
    student = models.ForeignKey(
        to='accounts.User',
        verbose_name='Студент',
        on_delete=models.CASCADE,
        related_name='mark_student',
        null=True,
    )
    subject = models.ForeignKey(
        Subject,
        verbose_name='Предмет',
        on_delete=models.CASCADE,
        related_name='mark_subject',
        null=True,
    )
    teacher = models.ForeignKey(
        to='accounts.User',
        verbose_name='Викладач',
        on_delete=models.CASCADE,
        related_name='mark_teacher',
        null=True,
    )
