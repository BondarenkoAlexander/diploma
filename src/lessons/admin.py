from django.contrib import admin

# Register your models here.
from lessons.models import Subject, Lesson, Mark


class LessonAdmin(admin.ModelAdmin):
    model = Lesson
    list_filter = ('subject', 'group')
    list_per_page = 5


admin.site.register(Subject)
admin.site.register(Mark)
admin.site.register(Lesson, LessonAdmin)
