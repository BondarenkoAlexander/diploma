from django.urls import path, include

from lessons.views import LessonCreateView, LessonUpdateView, LessonDeleteView, MarkUpdateView

app_name = 'lessons'

urlpatterns = [
    path('create/', LessonCreateView.as_view(), name='create'),
    path('update/<uuid:lesson_uuid>/', LessonUpdateView.as_view(), name='update'),
    path('delete/<uuid:lesson_uuid>/', LessonDeleteView.as_view(), name='delete'),
    path('mark/update/<int:mark_id>/', MarkUpdateView.as_view(), name='mark_update'),

]
